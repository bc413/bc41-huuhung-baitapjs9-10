function layThongTin(){
    let _taiKhoan = document.querySelector("#tknv").value;
    let _tenNV = document.querySelector("#name").value;
    let _email = document.querySelector("#email").value;
    let _matKhau = document.querySelector("#password").value;
    let _ngayLam = document.querySelector("#datepicker").value;
    let _luongCoBan = document.querySelector("#luongCB").value;
    let _chucVU = document.getElementById('chucvu').value;
    let _gioLam = document.querySelector("#gioLam").value;
    
    let nv = new NhanVien(_taiKhoan,_tenNV,_email,_matKhau,_ngayLam,_luongCoBan,_chucVU,_gioLam)
    return nv;
}

function nhanVienRow (nv) {
    return `<tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.tenNV}</td>
    <td>${nv.email}</td>
    <td>${nv.ngayLam}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tinhLuong()}</td>
    <td>${nv.xepLoai()}</td>
    <td class="d-flex">
        <button onclick="xoaNhanVien('${nv.taiKhoan}')" class="btn btn-danger mr-2">Xóa</button>
        <button onclick="suaNhanVien('${nv.taiKhoan}')" data-toggle="modal"
        data-target="#myModal" class="btn btn-warning" >Sửa</button>
    </td>
</tr>`
}

function renderNV(Arr){
    const HTML_RENDER = Arr.map(function(item) {

        return nhanVienRow(item)
    })
    document.querySelector("#tableDanhSach").innerHTML = HTML_RENDER.join("\n");
};


// Cách 1
function timKiemLoaiNV(DSNV){
    const INPUT = document.querySelector('#searchName')

    // Check điều kiện nếu có input thì mới add sự kiện
    INPUT && INPUT.addEventListener('input',function(e){
        // Chặn lại các sự kiện khác đang chạy
        e.preventDefault()
        let value = this.value
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf
        let result = DSNV.filter(item => {
            let XL = item.xepLoai() 
            return XL.toLocaleLowerCase('vn-VN').indexOf(value.toLocaleLowerCase('vn-VN')) !== -1
        })
        console.log(result)
        renderNV(result)
    })
};

// Cách 2
// function timNhanVien(DSNV){
//     let search = document.querySelector("#btnTimNV");
//     search.addEventListener("click",function(e){
//         e.preventDefault()
//         let valueXepLoai = document.querySelector("#searchName").value;
//         let resual = DSNV.filter(item =>{  
//             let arrnew = item.xepLoai()
//             console.log(arrnew)
//             let ketQua =  arrnew.toLocaleLowerCase('vn-VN').indexOf(valueXepLoai.toLocaleLowerCase('vn-VN'));
//             if(ketQua !== -1){
//                 return ketQua;
//             }
//         })
//         console.log(resual)
//         renderNV(resual)
//     })
// }