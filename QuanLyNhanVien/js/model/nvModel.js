function NhanVien(_taiKhoan,_tenNV,_email,_matKhau,_ngayLam,_luongCoBan,_chucVU,_gioLam){
    this.taiKhoan= _taiKhoan;
    this.tenNV= _tenNV;
    this.email= _email;
    this.matKhau= _matKhau;
    this.ngayLam= _ngayLam;
    this.luongCoBan= _luongCoBan;
    this.chucVu= _chucVU;
    this.gioLam = _gioLam;
    
    this.tinhLuong = function(){
        let soX = 0;
        if(this.chucVu == "Sếp"){
            soX = 3;
        }else if(this.chucVu == "Trưởng phòng" ){
            soX = 2;
        }else if(this.chucVu == "Nhân viên" ){
            soX = 1;
        }else{
            soX = 0;
        }
        let tongLuong = this.luongCoBan * soX
        return tongLuong
    };
    this.xepLoai = function(){
        if(this.gioLam <= 160){
            return "Nhân viên trung bình"
        }else if(this.gioLam >= 192){
            return "Nhân viên Xuất sắc"
        }else if(this.gioLam >= 176){
            return "nhân viên giỏi"
        }else{
            return " nhân viên khá"
        }
    }
};