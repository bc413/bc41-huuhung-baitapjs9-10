let DSNV=[]


let getDSNV = localStorage.getItem('arrDSNV');

if(getDSNV !=null){

    let arrData = JSON.parse(getDSNV)
    DSNV = arrData.map(function(item){
        let NV = new NhanVien(item.taiKhoan, item.tenNV, item.email, item.matKhau, item.ngayLam, item.luongCoBan, item.chucVu, item.gioLam)
        return NV;
    })
    renderNV(DSNV)
}


function themNhanVien(){
    layThongTin()
    
    // kt tk

    

    let isVaLid = kiemTraNhap("tknv","tbTKNV",0) &&  kiemTraKySo("tknv","tbTKNV",7) && ktChieuDai("Tài khoản","tknv", "tbTKNV",4,6) && kiemTraTRung("#tknv",DSNV, "tbTKNV");
    let isVaLid2 = kiemTraNhap("name","tbTen",1) && kiemTraTen("name", "tbTen")
    let isValid3 = kiemTraNhap("email","tbEmail",2) && kiemTraEmail("email", "tbEmail")
    let isValid4 = kiemTraNhap("password","tbMatKhau",3) &&  ktChieuDai("Mật khẩu","password","tbMatKhau", 6, 10) && kiemTraPass("password","tbMatKhau");
    let isValid5 = kiemTraNhap("luongCB","tbLuongCB",5) && ktLuong("luongCB","tbLuongCB");
    isValid5 = isValid5 & kiemTraNhap("datepicker","tbNgay",4);
    let isValid6 = kiemTraNhap("gioLam","tbGiolam",6) && soGioLam("gioLam","tbGiolam");
    isValid6 = isValid6 & kiemTraChucVu("chucvu","tbChucVu");


    if(isVaLid && isVaLid2 && isValid3 && isValid4 && isValid5 && isValid6){
        DSNV.push(layThongTin())
        let dsnvJSON = JSON.stringify(DSNV)
        localStorage.setItem('arrDSNV', dsnvJSON);
        renderNV(DSNV) 
    }
};


function xoaNhanVien(idMaNV){
    let viTri = timViTri(idMaNV,DSNV)
    if(viTri !== -1){
        DSNV.splice(viTri,1)
        renderNV(DSNV)
    }
    let dsnvJSON = JSON.stringify(DSNV)
    localStorage.setItem('arrDSNV', dsnvJSON);
}

function suaNhanVien(idMaNV){
    let viTri = timViTri(idMaNV,DSNV)
    if(viTri !== -1){
        let showAR = DSNV[viTri]
        document.getElementById("tknv").disabled = true;
        document.querySelector("#tknv").value = showAR.taiKhoan;
        document.querySelector("#name").value = showAR.tenNV;
        document.querySelector("#email").value = showAR.email;
        document.querySelector("#password").value = showAR.matKhau;
        document.querySelector("#datepicker").value = showAR.ngayLam;
        document.querySelector("#luongCB").value = showAR.luongCoBan;
        document.getElementById('chucvu').value = showAR.chucVu;
        document.querySelector("#gioLam").value = showAR.gioLam;
    }   
    // click sủa show thông tin lên form
    // $('#myModal').modal('show')
}

function capNhatNhaVien(){
    document.getElementById("tknv").disabled = true;
    let maNV = document.querySelector("#tknv").value
    let viTri = timViTri(maNV,DSNV)


    
    if(viTri != -1){
        DSNV[viTri].tenNV = document.querySelector("#name").value
        DSNV[viTri].email = document.querySelector("#email").value
        DSNV[viTri].matKhau = document.querySelector("#password").value
        DSNV[viTri].ngayLam = document.querySelector("#datepicker").value
        DSNV[viTri].luongCoBan = document.querySelector("#luongCB").value
        DSNV[viTri].chucVu =  document.getElementById('chucvu').value;
        DSNV[viTri].gioLam = document.querySelector("#gioLam").value
        // validate cập nhật
        let isVaLid = kiemTraNhap("tknv","tbTKNV",0) &&  kiemTraKySo("tknv","tbTKNV",7) && ktChieuDai("Tài khoản","tknv", "tbTKNV",4,6);
        let isVaLid2 = kiemTraNhap("name","tbTen",1) && kiemTraTen("name", "tbTen")
        let isValid3 = kiemTraNhap("email","tbEmail",2) && kiemTraEmail("email", "tbEmail")
        let isValid4 = kiemTraNhap("password","tbMatKhau",3) &&  ktChieuDai("Mật khẩu","password","tbMatKhau", 6, 10) && kiemTraPass("password","tbMatKhau");
        let isValid5 = kiemTraNhap("luongCB","tbLuongCB",5) && ktLuong("luongCB","tbLuongCB");
        isValid5 = isValid5 & kiemTraNhap("datepicker","tbNgay",4);
        let isValid6 = kiemTraNhap("gioLam","tbGiolam",6) && soGioLam("gioLam","tbGiolam");
        isValid6 = isValid6 & kiemTraChucVu("chucvu","tbChucVu");
        if(isVaLid && isVaLid2 && isValid3 && isValid4 && isValid5 && isValid6){
            renderNV(DSNV)
        }
        
    }
        let dsnvJSON = JSON.stringify(DSNV)
        localStorage.setItem('arrDSNV', dsnvJSON);
}

timKiemLoaiNV(DSNV)
// timNhanVien(DSNV)